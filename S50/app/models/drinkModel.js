// B1: Import mongoJS
const mongoose = require('mongoose');

//Khai báo Schema
const Schema = mongoose.Schema;

//Khởi tạo 1 schema với các thuộc tính được yêu cầu
const drinkSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia: {
        type: Number,
        required: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

//  B4: Export ra 1 model nhờ Schema vừa khai báo
module.exports = mongoose.model("drink", drinkSchema);