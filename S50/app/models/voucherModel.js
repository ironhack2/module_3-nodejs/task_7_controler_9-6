//  Import mongoJS
const mongoose = require('mongoose');

// Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo 1 schema với các thuộc tính
const voucherSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    maVoucher: {
        type: String,
        unique: true,
        required: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

// Export ra 1 model nhờ Schema vừa khai báo
module.exports = mongoose.model("voucher", voucherSchema);