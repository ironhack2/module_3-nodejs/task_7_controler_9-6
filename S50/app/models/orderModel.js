//Import mongoJS
const mongoose = require('mongoose');

//Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Khởi tạo 1 schema với các thuộc tính
const orderSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: [{
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    }],
    drink: [{
        type: mongoose.Types.ObjectId,
        ref: "Drink"
    }],
    status: {
        type: String,
        required: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

//Export ra 1 model nhờ Schema vừa khai báo
module.exports = mongoose.model('order', orderSchema);