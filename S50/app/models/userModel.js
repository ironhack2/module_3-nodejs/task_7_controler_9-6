//  Import mongoJS
const mongoose = require('mongoose');

// Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo 1 schema với các thuộc tính
const userSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    fullName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        required: true
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }],
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
})

// Export ra 1 module nhờ Schema vừa khai báo
module.exports = mongoose.model("user", userSchema);