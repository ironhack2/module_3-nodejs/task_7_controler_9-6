//khai báo thư viện express
const express = require('express');
const { drinkMiddleware } = require('../middlewares/drinkMiddleware');


//Import course Controller
const { getAllDrink, getDrinkById, createDrink, updateDrinkById, deleteDrinkById } = require('../controllers/drinkController');

//tạo router
const drinkRouter = express.Router();

//sủ dụng middle ware
drinkRouter.use(drinkMiddleware);

// Sử dụng middleware
drinkRouter.get('/drinks', drinkMiddleware, getAllDrink);

drinkRouter.get('/drinks/:drinkId', drinkMiddleware, getDrinkById);

drinkRouter.post('/drinks', drinkMiddleware, createDrink);

drinkRouter.put('/drinks/:drinkId', drinkMiddleware, updateDrinkById);

drinkRouter.delete('/drinks/:drinkId', drinkMiddleware, deleteDrinkById);


module.exports = drinkRouter;