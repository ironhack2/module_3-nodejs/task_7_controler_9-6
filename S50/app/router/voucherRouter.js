//khai báo thư viện express
const express = require('express');
const { voucherMiddleware } = require('../middlewares/voucherMiddleware');


const { getAllVoucher, getVoucherById, createVoucher, updateVoucherById, deleteVoucherById } = require('../controllers/voucherController');


//tạo router
const voucherRouter = express.Router();

//sủ dụng middle ware
voucherRouter.use(voucherMiddleware);

//get all voucher
voucherRouter.get('/voucher', getAllVoucher);

//get a voucher
voucherRouter.get('/voucher/:voucherId', getVoucherById);

//create a voucher
voucherRouter.post('/voucher', createVoucher);

//update a voucher
voucherRouter.put('/voucher/:voucherId', updateVoucherById);

//delete a voucher
voucherRouter.delete('/voucher/:voucherId', deleteVoucherById);


module.exports = { voucherRouter };