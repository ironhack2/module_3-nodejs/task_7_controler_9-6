//  Import Voucher model vào controller
const voucherModel = require('../models/voucherModel');

//Khai báo thư viện mongoose
const mongoose = require('mongoose');


// LẤY TẤT CẢ VOUCHER
const getAllVoucher = (request, response) => {

    //b1: thu thập dữ liệu
    //b2: validate dữ liệu
    //b3: thao tác với cơ sở dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get all voucher success",
                data: data
            })
        }
    })
}


// LẤY VOUCHER THEO ID
const getVoucherById = (request, response) => {

    //b1: thu thập dữ liệu
    let voucherId = request.params.voucherId;

    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        response.status(400).json({
            status: "Error 400: bad request",
            message: "Voucher ID is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get voucher by id success",
                data: data
            })
        }
    })
}


//CREATE A VOUCHER
const createVoucher = (request, response) => {

    //b1: thu thập dữ liệu
    let bodyRequest = request.body;

    //b2: validate dữ liệu
    if (!bodyRequest.maVoucher) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "maNuocUong is require"
        })
    }

    if (!(Number.isInteger(bodyRequest.phanTramGiamGia)) && bodyRequest.phanTramGiamGia > 0) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "phanTramGiamGia is not valid"
        })
    }

    //b3: thao tác với cơ sở dữ liệu
    let createVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: bodyRequest.maVoucher,
        phanTramGiamGia: bodyRequest.phanTramGiamGia,
        ghiChu: bodyRequest.ghiChu
    }

    voucherModel.create(createVoucher, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Create voucher success",
                data: data
            })
        }
    })

}



//UPDATE A VOUCHER
const updateVoucherById = (request, response) => {

    //b1: thu thập dữ liệu
    let voucherId = request.params.voucherId;
    let voucherBody = request.body;

    //b2: thu thập dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "drink id is not valid"
        })
    }

    //b3: thao tác với cơ sở dữ liệu
    let voucherUpdate = {
        maVoucher: voucherBody.maVoucher,
        phanTramGiamGia: voucherBody.phanTramGiamGia,
        ghiChu: voucherBody.ghiChu
    }

    voucherModel.findByIdAndUpdate(voucherId, voucherUpdate, (error, data) => {

        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update drink success",
                data: data
            })
        }
    })
}



// DELETE A VOUCHER
const deleteVoucherById = (request, response) => {

    //b1: thu thập dữ liệu
    let voucherId = request.params.voucherId;

    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "drink id is not valid"
        })
    }


    //b3: thao tác với cơ sở dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {

        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update drink success",
                data: data
            })
        }
    })
}

//   Export controller thành 1 module là 1 object
module.exports = {
    getAllVoucher: getAllVoucher,
    getVoucherById: getVoucherById,
    createVoucher: createVoucher,
    updateVoucherById: updateVoucherById,
    deleteVoucherById: deleteVoucherById,
}

// {
//     "maVoucher": "123",
//     "phanTramGiamGia": 10,
//     "ghiChu": "giảm giá 10%"
// }