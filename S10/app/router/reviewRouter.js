//  Khai báo thư viện express
const express = require('express');

//Import courseMiddleware
const reviewMiddleware = require('../middleware/reviewMiddleware');

//Import course Controller
const { createReview, getAllReview, getReviewById, updateReviewById, deleteReviewById } = require('../controllers/reviewController');

//tạo router
const reviewRouter = express.Router();

//sủ dụng middle ware
reviewRouter.use(reviewMiddleware);

reviewRouter.get('/review', getAllReview);
reviewRouter.post('/review', createReview);
reviewRouter.get('/review/:reviewId', getReviewById);
reviewRouter.put('/review/:reviewId', updateReviewById);
reviewRouter.delete('/review/:reviewId', deleteReviewById);


//Export
module.exports = { reviewRouter };