//  Import course model vào controllers
const reviewModel = require('../models/reviewModel');

//Khai báo thư viện mongoose
const mongoose = require('mongoose');

const createReview = (request, response) => {

    //B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.title) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.student) && bodyRequest.student > 0)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "No Student is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createReview = {
        _id: mongoose.Types.ObjectId(),
        title: bodyRequest.title,
        description: bodyRequest.description,
        noStudent: bodyRequest.student
    }

    reviewModel.create(createReview, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course Created",
                data: data
            })
        }
    })
}

const getAllReview = (request, response) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    reviewModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course Created",
                data: data
            })
        }
    })
}

const getReviewById = (request, response) => {
    //B1: Thu thập dữ liệu
    let reviewId = request.params.reviewId;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    reviewModel.findById(reviewId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Review Created",
                data: data
            })
        }
    })
}

const updateReviewById = (request, response) => {
    //B1: Thu thập dữ liệu
    let reviewId = request.params.reviewId;
    let bodyRequest = request.body;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let reviewUpdate = {
        title: bodyRequest.title,
        description: bodyRequest.description,
        noStudent: bodyRequest.student
    }

    reviewModel.findByIdAndUpdate(reviewId, reviewUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Review Created",
                data: data
            })
        }
    })
}

const deleteReviewById = (request, response) => {
    //B1: Thu thập dữ liệu
    let reviewId = request.params.reviewId;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    reviewModel.findByIdAndDelete(reviewId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(204).json({
                status: "Success: Delete Review success"
            })
        }
    })

}

//   Export controller thành 1 module là 1 object
module.exports = {
    createReview: createReview,
    getAllReview: getAllReview,
    getReviewById: getReviewById,
    updateReviewById: updateReviewById,
    deleteReviewById: deleteReviewById,
}