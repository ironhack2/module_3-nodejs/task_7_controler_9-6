//  Import course model vào controllers
const courseModel = require("../models/courseModel");

//Khai báo thư viện mongoose
const mongoose = require('mongoose');


//Create
const createCourse = (request, response) => {

    //B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.title) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.student) && bodyRequest.student > 0)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "No Student is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createCourse = {
        _id: mongoose.Types.ObjectId(),
        title: bodyRequest.title,
        description: bodyRequest.description,
        noStudent: bodyRequest.student
    }

    courseModel.create(createCourse, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course Created",
                data: data
            })
        }
    })
}

//get all Course
const getAllCourse = (request, response) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    courseModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course Created",
                data: data
            })
        }
    })
}

const getCourseById = (request, response) => {
    //B1: Thu thập dữ liệu
    let courseId = request.params.courseId;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    courseModel.findById(courseId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Course Created",
                data: data
            })
        }
    })
}

const updateCourseById = (request, response) => {
    //B1: Thu thập dữ liệu
    let courseId = request.params.courseId;
    let bodyRequest = request.body;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let courseUpdate = {
        title: bodyRequest.title,
        description: bodyRequest.description,
        noStudent: bodyRequest.student
    }

    courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course Created",
                data: data
            })
        }
    })
}

const deleteCourseById = (request, response) => {
    //B1: Thu thập dữ liệu
    let courseId = request.params.courseId;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(204).json({
                status: "Success: Delete course success"
            })
        }
    })

}

//   Export controller thành 1 module là 1 object
module.exports = {
    createCourse: createCourse,
    getAllCourse: getAllCourse,
    getCourseById: getCourseById,
    updateCourseById: updateCourseById,
    deleteCourseById: deleteCourseById,
}