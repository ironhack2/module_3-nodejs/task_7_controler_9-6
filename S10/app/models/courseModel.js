//  Import mongoJS
const mongoose = require('mongoose');

//  Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//  Khởi tạo 1 schema với các thuộc tính được yêu cầu
const courseSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
    review: [{
        type: mongoose.Types.ObjectId,
        ref: "review"
    }]
});

//  B4:  Export ra 1 model nhờ Schema vừa khai báo
module.exports = mongoose.model("course", courseSchema);