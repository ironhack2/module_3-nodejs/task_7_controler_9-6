//Khai báo thư viện express
const express = require('express');

// Import mongooseJS
const mongoose = require('mongoose');


const courseRouter = require('./app/router/courseRouter');
const { reviewRouter } = require('./app/router/reviewRouter');

//Khai báo app nodeJS
const app = new express();


//Khai báo middleware json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

//Khai báo cổng nodeJS
const port = 8000;


mongoose.connect("mongodb://localhost:27017/", (err) => {
    if (err) {
        throw err;
    }

    console.log("Connect MongoDB successfully!");
})


//Khai báo API
app.get('/', (request, response) => {
    let today = new Date();
    console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`);

    response.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
    })
})

//Sử dụng Router
app.use('/', courseRouter);
app.use('/', reviewRouter);

//chạy trên cổng nodeJS
app.listen(port, () => {
    console.log(`listening on port ${port}`);
})